
package com.gulp.model.openmapcharge;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenChargeMapResult {
    @JsonProperty("ID")
    private Integer ID;
    @JsonProperty("UUID")
    private String UUID;
    @JsonProperty("AddressInfo")
    private com.gulp.model.openmapcharge.AddressInfo AddressInfo;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public com.gulp.model.openmapcharge.AddressInfo getAddressInfo() {
        return AddressInfo;
    }

    public void setAddressInfo(com.gulp.model.openmapcharge.AddressInfo addressInfo) {
        AddressInfo = addressInfo;
    }
}
