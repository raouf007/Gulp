
package com.gulp.model.openmapcharge;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressInfo {

    @JsonProperty("Title")
    private String Title;
    @JsonProperty("AddressLine1")
    private String AddressLine1;
    @JsonProperty("AddressLine2")
    private Object AddressLine2;
    @JsonProperty("Town")
    private String Town;
    @JsonProperty("ContactTelephone1")
    private Object ContactTelephone1;
    @JsonProperty("Distance")
    private Double Distance;
    @JsonProperty("DistanceUnit")
    private Integer DistanceUnit;
    @JsonProperty("Latitude")
    private Double Latitude;
    @JsonProperty("Longitude")
    private Double Longitude;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        AddressLine1 = addressLine1;
    }

    public Object getAddressLine2() {
        return AddressLine2;
    }

    public void setAddressLine2(Object addressLine2) {
        AddressLine2 = addressLine2;
    }

    public String getTown() {
        return Town;
    }

    public void setTown(String town) {
        Town = town;
    }

    public Object getContactTelephone1() {
        return ContactTelephone1;
    }

    public void setContactTelephone1(Object contactTelephone1) {
        ContactTelephone1 = contactTelephone1;
    }

    public Double getDistance() {
        return Distance;
    }

    public void setDistance(Double distance) {
        Distance = distance;
    }

    public Integer getDistanceUnit() {
        return DistanceUnit;
    }

    public void setDistanceUnit(Integer distanceUnit) {
        DistanceUnit = distanceUnit;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }
}
