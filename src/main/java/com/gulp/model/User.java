package com.gulp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raouf BEN AOUISSI on 06/10/2015.
 * raouf.ben.aouissi@gmail.com
 */

@Entity
@Table(name = "user")
public class User extends BaseEntity<Long> {

	@Id
    @GeneratedValue
	@Column(name = "user_id", unique = true, nullable = false)
	private Long userId;
	
	@Column(name = "name", length = 100)
	private String name;

	@Column(name = "phone_number", length = 100)
	private String phoneNumber;

	@Column(name = "email", length = 100)
	private String email;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
	private List<Terminal> terminals = new ArrayList<Terminal>(0);

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "renter")
	private List<Reservation> reservation = new ArrayList<Reservation>(0);

	public User() {

	}

	@Override
	public Long getId() {
		return userId;
	}

	@Override
	public void callLazyLoads() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLazyLoadsToNull() {
		// TODO Auto-generated method stub
		
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Terminal> getTerminals() {
		return terminals;
	}

	public void setTerminals(List<Terminal> terminals) {
		this.terminals = terminals;
	}

	public List<Reservation> getReservation() {
		return reservation;
	}

	public void setReservation(List<Reservation> reservation) {
		this.reservation = reservation;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}