
package com.gulp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * Created by Raouf BEN AOUISSI on 06/10/2015.
 * raouf.ben.aouissi@gmail.com
 */

public abstract class BaseEntity<I> implements Cloneable, Serializable{

    @JsonIgnore
    public abstract I getId();
    public abstract void callLazyLoads();
    public abstract void setLazyLoadsToNull();
	public Object clone() {
    	Object o = null;
    	try {
      		o = super.clone();
    	} catch(CloneNotSupportedException ex) {
      		ex.printStackTrace();
	    }
	    return o;
  	}
}
