package com.gulp.model;

import javax.persistence.*;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.ben.aouissi@gmail.com
 */
@Entity
@Table(name = "terminal")
public class Terminal extends BaseEntity<Long> {

    @Id
    @GeneratedValue
    @Column(name = "terminal_id", unique = true, nullable = false)
    private Long terminalId;

    private String place;

    private double longitude;

    private double latitude;

    private double price;

    private String type;

    private double distance;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User owner;

    @Override
    public Long getId() {
        return terminalId;
    }

    @Override
    public void callLazyLoads() {

    }

    @Override
    public void setLazyLoadsToNull() {

    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}