package com.gulp.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.ben.aouissi@gmail.com
 */
@Entity
@Table(name = "reservation")
public class Reservation extends BaseEntity<Long>{
    @Id
    @GeneratedValue
    @Column(name = "reservationId", unique = true, nullable = false)
    private Long reservationId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false, length = 19)
    private Date date;

    private int duration;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "terminal_id")
    private Terminal terminal;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "renter_id")
    private User renter;

    @Override
    public Long getId() {
        return reservationId;
    }

    @Override
    public void callLazyLoads() {

    }

    @Override
    public void setLazyLoadsToNull() {

    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public User getRenter() {
        return renter;
    }

    public void setRenter(User renter) {
        this.renter = renter;
    }
}
