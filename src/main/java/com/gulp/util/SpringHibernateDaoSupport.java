package com.gulp.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.inject.Inject;

/**
 * Created by Raouf BEN AOUISSI on 06/10/2015.
 * raouf.ben.aouissi@gmail.com
 */

public abstract class SpringHibernateDaoSupport{
	
	private SessionFactory sessionFactory;
	
    @Inject
    public void initSession(SessionFactory sessionFactory){
       this.sessionFactory = sessionFactory;
    }

    public Session getSession(){
        return sessionFactory.getCurrentSession();
    }
}