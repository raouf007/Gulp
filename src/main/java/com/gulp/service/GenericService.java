package com.gulp.service;

import com.gulp.dao.GenericDao;

import java.sql.Connection;
import java.util.List;


/**
 * Created by Raouf BEN AOUISSI on 06/10/2015.
 * raouf.ben.aouissi@gmail.com
 */

public interface GenericService<T, I> {

    public GenericDao<T, I> getDao();

    public T get(I primaryKey);

    public List<T> getAll();

    public List<T> getAll(int firstResult, int maxResult);

    public T getOneBy(String paramName, Object paramValue);

    public List<T> getAllBy(String paramName, Object paramValue);

    public List<T> getAllBy(String paramName, Object paramValue, int firstResult, int maxResult, String order, String orderWay);

    public I save(T entity);

    public void delete(I primaryKey);

    public void update(T entity);
}