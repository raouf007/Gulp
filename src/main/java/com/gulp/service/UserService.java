package com.gulp.service;

import com.gulp.model.User;

/**
 * Created by Raouf BEN AOUISSI on 29/06/2014.
 * raouf.ben.aouissi@gmail.com
 */

public interface UserService extends GenericService<User, Long> {

}