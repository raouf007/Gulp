package com.gulp.service.impl;

import com.gulp.dao.GenericDao;
import com.gulp.service.GenericService;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.util.List;

/**
 * Created by Raouf BEN AOUISSI on 06/10/2015.
 * raouf.ben.aouissi@gmail.com
 */
@Transactional
public abstract class GenericServiceImpl<T, I> implements GenericService<T, I> {

    
    public T get(I primaryKey) {
        return (T) getDao().find(primaryKey);
    }

    
    public List<T> getAll() {
        return (List<T>) getDao().findAll();
    }

    @Override
    public List<T> getAll(int firstResult, int maxResult) {
        return getDao().findAll(firstResult, maxResult);
    }

    @Override
    public List<T> getAllBy(String paramName, Object paramValue, int firstResult, int maxResult, String order, String orderWay){
        return getDao().findAllBy(paramName, paramValue, firstResult, maxResult, order, orderWay);
    }

    public I save(T entity) {
        return (I) getDao().save(entity);
    }

    public void delete(I primaryKey) {
        T entity = getDao().find(primaryKey);
        if (entity != null){
            getDao().delete(entity);
        }
    }

    @Override
    public T getOneBy(String paramName, Object paramValue) {
        return getDao().findOneBy(paramName, paramValue);
    }

    @Override
    public List<T> getAllBy(String paramName, Object paramValue) {
        return getDao().findAllBy(paramName, paramValue);
    }

    public void update(T entity) {
        getDao().update(entity);
    }

    public abstract GenericDao<T, I> getDao();
}