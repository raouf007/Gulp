package com.gulp.service.impl;

import com.gulp.dao.GenericDao;
import com.gulp.dao.ReservationDao;
import com.gulp.model.Reservation;
import com.gulp.service.ReservationService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.ben.aouissi@gmail.com
 */
@Service
public class ReservationServiceImpl extends GenericServiceImpl<Reservation, Long> implements ReservationService {
    private ReservationDao reservationDao;

    @Override
    public GenericDao<Reservation, Long> getDao() {
        return reservationDao;
    }

    @Inject
    public void setReservationDao(ReservationDao reservationDao) {
        this.reservationDao = reservationDao;
    }

    public List<Reservation> getByTerminal(Long terminalId) {
        return reservationDao.findByTerminal(terminalId);
    }

    public List<Reservation> getByUser(Long userId) {
        return reservationDao.findByUser(userId);
    }
}
