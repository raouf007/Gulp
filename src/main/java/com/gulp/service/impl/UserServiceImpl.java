package com.gulp.service.impl;

import com.gulp.dao.GenericDao;
import com.gulp.dao.UserDao;
import com.gulp.model.User;
import com.gulp.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Raouf BEN AOUISSI on 29/06/2014.
 * raouf.ben.aouissi@gmail.com
 */
@Service
public class UserServiceImpl extends GenericServiceImpl<User, Long> implements UserService {

    private UserDao userDao;

    @Override
    public GenericDao<User, Long> getDao() {
        return userDao;
    }

    @Inject
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

}