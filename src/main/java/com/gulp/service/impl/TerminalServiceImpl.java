package com.gulp.service.impl;

import com.gulp.dao.GenericDao;
import com.gulp.dao.TerminalDao;
import com.gulp.dto.TerminalDto;
import com.gulp.model.Terminal;
import com.gulp.model.openmapcharge.OpenChargeMapResult;
import com.gulp.service.GenericService;
import com.gulp.service.TerminalService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.benaouissi@smile.fr
 */
@Service
public class TerminalServiceImpl extends GenericServiceImpl<Terminal, Long> implements TerminalService{

    private TerminalDao terminalDao;
    private RestTemplate restTemplate = new RestTemplate();

    static String url = "http://api.openchargemap.io/v2/poi/?output=json&maxresults=10&longitude=";

    public List<Terminal> getNearTerminals(double longitude, double latitude) {

        List<Terminal> local = terminalDao.findNearTerminals(longitude, latitude);

        ResponseEntity<OpenChargeMapResult[]> responseEntity = restTemplate.getForEntity(url + longitude + "&latitude=" + latitude, OpenChargeMapResult[].class);
        List<OpenChargeMapResult> apiList = Arrays.asList(responseEntity.getBody());

        for(OpenChargeMapResult item : apiList){
            Terminal terminal = new Terminal();
            terminal.setPlace(item.getAddressInfo().getAddressLine1()+" "+item.getAddressInfo().getTown());
            terminal.setLongitude(item.getAddressInfo().getLongitude());
            terminal.setLatitude(item.getAddressInfo().getLatitude());
            terminal.setDistance(item.getAddressInfo().getDistance());
            local.add(terminal);
        }
        Collections.sort(local, new Comparator<Terminal>() {
            @Override
            public int compare(Terminal o1, Terminal o2) {
                if(o1.getDistance()>o2.getDistance())return 1;
                return -1;
            }
        });

        return local;
    }

    @Override
    public GenericDao<Terminal, Long> getDao() {
        return terminalDao;
    }

    @Inject
    public void setTerminalDao(TerminalDao terminalDao) {
        this.terminalDao = terminalDao;
    }

    public static void main(String[] a){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<OpenChargeMapResult[]> responseEntity = restTemplate.getForEntity(url + 0.25 + "&latitude=" + 0.5, OpenChargeMapResult[].class);
        List<OpenChargeMapResult> apiList = Arrays.asList(responseEntity.getBody());
        System.out.println();
    }
}