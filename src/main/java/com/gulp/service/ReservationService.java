package com.gulp.service;

import com.gulp.model.Reservation;

import java.util.List;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.ben.aouissi@gmail.com
 */
public interface ReservationService extends GenericService<Reservation, Long> {
    public List<Reservation> getByTerminal(Long terminalId);
    public List<Reservation> getByUser(Long userId);
}
