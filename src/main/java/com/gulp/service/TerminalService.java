package com.gulp.service;

import com.gulp.model.Terminal;

import java.util.List;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.benaouissi@smile.fr
 */
public interface TerminalService extends GenericService<Terminal, Long> {
    public List<Terminal> getNearTerminals(double longitude, double latitude);
}
