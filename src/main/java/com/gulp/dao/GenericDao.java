package com.gulp.dao;


import java.sql.Connection;
import java.util.List;


/**
 * Created by Raouf BEN AOUISSI on 06/10/2015.
 * raouf.ben.aouissi@gmail.com
 */

public interface GenericDao<T, I> {

    public Class<T> getEntityClass();

    public List<T> findAll();

    public List<T> findAll(int firstResult, int maxResult);

    public List<T> findAllBy(String paramName, Object paramValue);

    public List<T> findAllBy(String paramName, Object paramValue, int firstResult, int maxResult, String order, String orderWay);

    public T findOneBy(String paramName, Object paramValue);

    public T find(I primaryKey);

    public I save(T entity);

    public void delete(T entity);

    public void update(T entity);

}