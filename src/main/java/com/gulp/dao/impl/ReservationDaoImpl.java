package com.gulp.dao.impl;

import com.gulp.dao.ReservationDao;
import com.gulp.model.Reservation;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.benaouissi@smile.fr
 */

@Repository
public class ReservationDaoImpl extends GenericDaoImpl<Reservation, Long> implements ReservationDao {

    public List<Reservation> findByTerminal(Long terminalId) {
        List<Reservation> reservations = (List<Reservation>) getSession().createQuery("from Reservation r where r.terminal.terminalId=" + terminalId).list();
        return reservations;
    }

    public List<Reservation> findByUser(Long userId) {
        List<Reservation> reservations = (List<Reservation>) getSession().createQuery("from Reservation r where r.user.userId=" + userId).list();
        return reservations;
    }
}
