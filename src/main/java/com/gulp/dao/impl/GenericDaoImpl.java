package com.gulp.dao.impl;

import com.gulp.dao.GenericDao;
import com.gulp.model.BaseEntity;
import com.gulp.util.SpringHibernateDaoSupport;
import org.hibernate.criterion.Restrictions;
import org.hibernate.internal.SessionImpl;
import org.hibernate.internal.util.SerializationHelper;
import org.springframework.transaction.annotation.Transactional;

import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Raouf BEN AOUISSI on 06/10/2015.
 * raouf.ben.aouissi@gmail.com
 */

@Transactional
public abstract class GenericDaoImpl<T, I extends Serializable> extends SpringHibernateDaoSupport implements GenericDao<T, I> {

    public Class<T> getEntityClass() {
        ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
        return (Class<T>) parameterizedType.getActualTypeArguments()[0];
    }

    public T find(I primaryKey) {
    	T objectToFind = (T) getSession().get(getEntityClass(), primaryKey);
        if(objectToFind != null)
        	((BaseEntity<I>)objectToFind).callLazyLoads();
        return objectToFind;
    }

    public List<T> findAll() {
       return (List<T>) getSession().createQuery("from " + getEntityClass().getName()).list();
    }

    @Override
    public List<T> findAll(int firstResult, int maxResult) {
        return (List<T>) getSession().createQuery("from " + getEntityClass().getName()).setFirstResult(firstResult).setMaxResults(maxResult).list();
    }

    @Override
    public List<T> findAllBy(String paramName, Object paramValue, int firstResult, int maxResult, String order, String orderWay) {
        return (List<T>) getSession().createQuery("from " + getEntityClass().getName()+" e where e."+paramName+"=:param1 order by e."+order+" "+orderWay).setParameter("param1", paramValue).setFirstResult(firstResult).setMaxResults(maxResult).list();
    }
    
    @Override
    public List<T> findAllBy(String paramName, Object paramValue) {
        return (List<T>) getSession().createQuery("from " + getEntityClass().getName() + " e where e."+paramName+"=?").setParameter(0, paramValue).list();
    }

    @Override
    public T findOneBy(String paramName, Object paramValue) {
        return (T) getSession().createQuery("from " + getEntityClass().getName()+" e where e."+paramName+"=?").setParameter(0, paramValue).uniqueResult();
    }

    public I save(T entity) {
        return (I) getSession().save(entity);
    }

    public void delete(T entity) {
        getSession().delete(entity);
    }

    public void update(T entity) {
        getSession().update(entity);
    }


}