package com.gulp.dao.impl;

import com.gulp.dao.GenericDao;
import com.gulp.dao.TerminalDao;
import com.gulp.dao.UserDao;
import com.gulp.model.Terminal;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raouf BEN AOUISSI on 07/10/2015.
 * raouf.ben.aouissi@gmail.com
 */
@Repository
public class TerminalDaoImpl extends GenericDaoImpl<Terminal, Long> implements TerminalDao {

    private UserDao userDao;

    @Override
    public Long save(Terminal entity) {
        entity.setOwner(userDao.find(entity.getOwner().getId()));
        return super.save(entity);
    }

    public List<Terminal> findNearTerminals(double longitude, double latitude){
        List<Terminal> terminals = new ArrayList<>();
        List<Object[]> entities = getSession().createQuery("select t, (haversine(t.latitude, t.longitude, :latitude, :longitude) * 111.045) as distance from Terminal t order by distance").setParameter("latitude", latitude).setParameter("longitude", longitude).setMaxResults(10).list();
        for (Object[] entity : entities) {
            Terminal t = new Terminal();
            int i = 0;
            for (Object entityCol : entity) {
                if(entityCol instanceof Terminal){
                    t.setLongitude( ( (Terminal)entityCol).getLongitude());
                    t.setLatitude(((Terminal) entityCol).getLatitude());
                    t.setPrice(((Terminal) entityCol).getPrice());
                    t.setPlace(((Terminal) entityCol).getPlace());
                    t.setTerminalId(((Terminal) entityCol).getTerminalId());
                    t.setOwner(((Terminal) entityCol).getOwner());
                }else if(entityCol instanceof Double)
                    t.setDistance((double) entityCol);
            }
            terminals.add(t);
        }
        return terminals;
    }

    @Inject
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
