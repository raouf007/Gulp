package com.gulp.dao.impl;

import com.gulp.dao.UserDao;
import com.gulp.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by Raouf BEN AOUISSI on 29/06/2014.
 * raouf.ben.aouissi@gmail.com
 */
@Repository
public class UserDaoImpl extends GenericDaoImpl <User, Long> implements UserDao{
	
}