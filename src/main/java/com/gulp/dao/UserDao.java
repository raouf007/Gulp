package com.gulp.dao;

import com.gulp.model.User;

import java.util.List;


/**
 * Created by Raouf BEN AOUISSI on 29/06/2014.
 * raouf.ben.aouissi@gmail.com
 */

public interface UserDao extends GenericDao<User, Long> {

}
