package com.gulp.dao;

import com.gulp.dto.TerminalDto;
import com.gulp.model.Terminal;

import java.util.List;

/**
 * Created by Raouf BEN AOUISSI on 07/10/2015.
 * raouf.ben.aouissi@gmail.com
 */
public interface TerminalDao extends GenericDao<Terminal, Long> {
    public List<Terminal> findNearTerminals(double longitude, double latitude);
}
