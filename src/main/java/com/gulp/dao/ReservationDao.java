package com.gulp.dao;

import com.gulp.model.Reservation;

import java.util.List;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.benaouissi@smile.fr
 */
public interface ReservationDao extends GenericDao<Reservation, Long> {
    public List<Reservation> findByTerminal(Long terminalId);
    public List<Reservation> findByUser(Long userId);
}
