package com.gulp.controller.rest;

import com.gulp.model.BaseEntity;
import com.gulp.model.Reservation;
import com.gulp.model.User;
import com.gulp.service.GenericService;
import com.gulp.service.ReservationService;
import com.gulp.service.TerminalService;
import com.gulp.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.benaouissi@smile.fr
 */
@RestController
@RequestMapping(value = "/reservation")
public class ReservationRestController extends GenericRestController<Reservation, Long>{

    private ReservationService reservationService;
    private TerminalService terminalService;
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public long add(@RequestParam long terminalId, @RequestParam long renterId, @RequestParam long date, @RequestParam int duration) {
        Reservation reservation = new Reservation();
        reservation.setTerminal(terminalService.get(terminalId));
        reservation.setRenter(userService.get(renterId));
        reservation.setDate(new Date(date));
        reservation.setDuration(duration);
        return reservationService.save(reservation);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public long edit(@RequestParam Long id, @RequestParam String nom, @RequestParam String mail, @RequestParam String tel) {
        User user = userService.get(id);
        user.setName(nom);
        user.setEmail(mail);
        user.setPhoneNumber(tel);
        return userService.save(user);
    }


    @RequestMapping(value = "/terminal/{terminalId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Reservation> getByTerminal(@PathVariable Long terminalId) {

        List<Reservation> list;
        list = reservationService.getByTerminal(terminalId);
        return list;
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Reservation> getByUser(@PathVariable Long userId) {

        List<Reservation> list;
        list = reservationService.getByTerminal(userId);
        return list;
    }

    @Inject
    public void setReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @Inject
    public void setTerminalService(TerminalService terminalService) {
        this.terminalService = terminalService;
    }

    @Inject
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public GenericService<Reservation, Long> getService() {
        return reservationService;
    }


}
