package com.gulp.controller.rest;

import com.gulp.model.User;
import com.gulp.service.GenericService;
import com.gulp.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

import java.util.List;

/**
 * Created by Raouf BEN AOUISSI on 07/09/2015.
 * raouf.ben.aouissi@gmail.com
 */

@RestController
@RequestMapping(value = "/user")
public class UserRestController extends GenericRestController<User, Long>{

	private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public long add(@RequestParam String nom, @RequestParam String mail, @RequestParam String tel) {
        User user = new User();
        user.setName(nom);
        user.setEmail(mail);
        user.setPhoneNumber(tel);
        return userService.save(user);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public long edit(@RequestParam Long id, @RequestParam String nom, @RequestParam String mail, @RequestParam String tel) {
        User user = userService.get(id);
        user.setName(nom);
        user.setEmail(mail);
        user.setPhoneNumber(tel);
        return userService.save(user);
    }

    @Override
    public GenericService<User, Long> getService() {
        return userService;
    }

    @Inject
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}