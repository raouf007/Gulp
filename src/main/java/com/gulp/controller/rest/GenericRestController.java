package com.gulp.controller.rest;


import com.gulp.model.BaseEntity;
import com.gulp.service.GenericService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Raouf BEN AOUISSI on 06/10/2015.
 * raouf.ben.aouissi@gmail.com
 */

public abstract class GenericRestController<T, I>{

    public abstract GenericService<T, I> getService();
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public T get(@PathVariable ("id") I id) {
        T objectToFind = getService().get(id);
        //return new ResponseEntity<T>(objectToFind, HttpStatus.OK);
        return objectToFind;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<T> getAll(@RequestParam(required = false) Integer firstResult,
                                          @RequestParam(required = false) Integer maxResult) {

        List<T> list;
        if (firstResult != null && maxResult != null) {
            list = getService().getAll(firstResult, maxResult);
        } else {
            list = getService().getAll();
        }
        for(T entity : list){
        	((BaseEntity<I>)entity).setLazyLoadsToNull();
        }
        return list;
    }

    @RequestMapping(value = "/{entityId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable I entityId) {
        getService().delete(entityId);
    }

}