package com.gulp.controller.rest;

import com.gulp.model.Reservation;
import com.gulp.model.Terminal;
import com.gulp.model.User;
import com.gulp.service.GenericService;
import com.gulp.service.TerminalService;
import com.gulp.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by BEN AOUISSI Raouf on 07/10/15.
 * raouf.benaouissi@smile.fr
 */

@RestController
@RequestMapping(value = "/terminal")
public class TerminalRestController extends GenericRestController<Terminal, Long>{

    private TerminalService terminalService;
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public long add(@RequestParam long ownerId, @RequestParam double latitude, @RequestParam double longitude,
                    @RequestParam String type, @RequestParam double price, @RequestParam String place) {
        Terminal terminal = new Terminal();
        terminal.setOwner(userService.get(ownerId));
        terminal.setLatitude(latitude);
        terminal.setLongitude(longitude);
        terminal.setType(type);
        terminal.setPrice(price);
        terminal.setPlace(place);
        return terminalService.save(terminal);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public long edit(@RequestParam long terminalId,@RequestParam long ownerId, @RequestParam double latitude, @RequestParam double longitude,
                    @RequestParam String type, @RequestParam double price, @RequestParam String place) {
        Terminal terminal = terminalService.get(terminalId);
        terminal.setOwner(userService.get(ownerId));
        terminal.setLatitude(latitude);
        terminal.setLongitude(longitude);
        terminal.setType(type);
        terminal.setPrice(price);
        terminal.setPlace(place);
        return terminalService.save(terminal);
    }

    @RequestMapping(value = "/near", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Terminal> getNearTerminals(@RequestParam Double longitude, @RequestParam Double latitude) {

        List<Terminal> list;
        list = terminalService.getNearTerminals(longitude, latitude);
        return list;
    }

    @Override
    public GenericService<Terminal, Long> getService() {
        return terminalService;
    }

    @Inject
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Inject
    public void setTerminalService(TerminalService terminalService) {
        this.terminalService = terminalService;
    }
}