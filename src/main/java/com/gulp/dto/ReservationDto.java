package com.gulp.dto;

import java.util.Date;

/**
 * Created by Raouf BEN AOUISSI on 07/10/2015.
 * raouf.ben.aouissi@gmail.com
 */
public class ReservationDto {
    private long id;
    private long terminalId;
    private long renterId;
    private Date date;
    private int duration;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(long terminalId) {
        this.terminalId = terminalId;
    }

    public long getRenterId() {
        return renterId;
    }

    public void setRenterId(long renterId) {
        this.renterId = renterId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
