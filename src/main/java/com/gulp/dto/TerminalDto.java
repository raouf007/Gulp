package com.gulp.dto;

import com.gulp.model.Terminal;

/**
 * Created by Raouf BEN AOUISSI on 08/10/2015.
 * raouf.ben.aouissi@gmail.com
 */
public class TerminalDto {
    private Terminal terminal;
    private double distance;

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
